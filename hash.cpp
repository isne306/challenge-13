﻿#include<iostream>
#include"hash.h"

int Hash::inthash(int id)
{
    return id%50;
}

int Hash::strhash(string name)
{
    int hashValue = 0;
    for(unsigned int Pos=0;Pos<name.length();Pos++)
        {
        	hashValue+=int(name.at(Pos));
        }
    return (hashValue%50);
}

void Hash::addStudentint(list<student> hash[50], string name, int id)
{
    student *s = new student(name,id);
    int code = inthash(id);
    hash[code].push_back(*s);
}

void Hash::addStudentstr(list<student> hash[50], string name, int id)
{
    student *s = new student(name,id);
    int code = strhash(name);
    hash[code].push_back(*s);
}

string Hash::getstr(list<student> hash[50],int id)
{
    int code = inthash(id);
    list<student> liststu = hash[code];
    while(!liststu.empty())
    {
        student s = liststu.front();
        if(s.getID()==id)
        {
         	return s.getName();
        }
        liststu.pop_front();
    }
    return "Unknown";
}

int Hash::getint(list<student> hash[50],string name)
{
    int code = strhash(name);
    list<student> liststu = hash[code];
    while(!liststu.empty())
    {
        student s = liststu.front();
        if(s.getName()==name)
        {
            return s.getID();
        }
        liststu.pop_front();
    }
    return 0;
}