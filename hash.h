﻿#ifndef HASH
#define HASH
#include<list>
#include<string>
using namespace std;

class student
{
public:
	student(string name, int id)
	{ 
		NAME = name;
		ID = id;
	}
	void setID(int id)
	{
		ID = id;
	}
	void setName(string name)
	{
		NAME = name;
	}
	int getID() 
	{
		return ID;
	}
	string getName()
	{
		return NAME; 
	}

private:
	int ID;
	string NAME;
};

class Hash
{
public:
    int inthash(int);
    int strhash(string);

    void addStudentint(list<student> hash[50], string name, int id);
    void addStudentstr(list<student> hash[50], string name, int id);
    string getstr(list<student> hash[50],int id);

    int getint(list<student> hash[50],string name);
};
#endif //HASH
