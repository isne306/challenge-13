﻿#include <iostream>
#include"hash.h"
using namespace std;

int main()
{
    Hash h;
    
   	list<student> hash1[50];
	h.addStudentint(hash1, "ArtPii", 1006);
	h.addStudentint(hash1, "EarnEarn", 1007);
	cout << "#1) a hash function that will convert the student's ID to the students data" <<endl;
	cout << h.getstr(hash1, 1006) << endl; 
	cout << h.getstr(hash1, 1007) << endl; 
	cout << h.getstr(hash1, 1009) << endl;
	cout <<endl<<endl;

	list<student> hash2[50];
	h.addStudentstr(hash2, "Nick", 1009);
	h.addStudentstr(hash2, "Folk", 1011);
	cout << "#2) a hash function that will convert the student’s name to student’s ID" <<endl;
	cout << h.getint(hash2, "Nick") << endl;
	cout << h.getint(hash2, "Folk") << endl;
	cout << h.getint(hash2, "EarnEarn") << endl;
}